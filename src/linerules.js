const RomanNumber = require('roman-number');
var global = require('./global');

module.exports = {
    ruleVar: function (line) {
        if (line.search('glob is') >= 0) {
            line = line.split(' ').join('');
            global.glob = line.slice(line.length - 1, line.length);
        }
        if (line.search('prok is') >= 0) {
            line = line.split(' ').join('');
            global.prok = line.slice(line.length - 1, line.length);
        }
        if (line.search('pish is') >= 0) {
            line = line.split(' ').join('');
            global.pish = line.slice(line.length - 1, line.length);
        }
        if (line.search('tegj is') >= 0) {
            line = line.split(' ').join('');
            global.tegj = line.slice(line.length - 1, line.length);
        }



    },

    ruleMetals: function (line) {
        if (line.search('Silver is') >= 0) {
            var tempPrice = parseInt(line.split('Silver is')[1].split('Credits')[0]);
            var tempVal   = line.split('Silver is')[0].split(' ');
            var totalPrice = 0;
            for(i=0; i<tempVal.length-1; i++){
                if(tempVal[i] == 'glob'){
                    totalPrice += RomanNumber(global.glob).value;
                }
                if(tempVal[i] == 'prok'){
                    totalPrice += RomanNumber(global.prok).value;
                }
                if(tempVal[i] == 'pish'){
                    totalPrice += RomanNumber(global.pish).value;
                }
                if(tempVal[i] == 'tegj'){
                    totalPrice += RomanNumber(global.tegj).value;
                }
            }
            global.Silver = tempPrice / totalPrice;
            // console.log('SILVER -> ', global.Silver);
        }

        if (line.search('Gold is') >= 0) {
            var tempPrice = parseInt(line.split('Gold is')[1].split('Credits')[0]);
            var tempVal   = line.split('Gold is')[0].split(' ');
            var totalPrice = 0;
            for(i=0; i<tempVal.length-1; i++){
                if(tempVal[i] == 'glob'){
                    totalPrice += RomanNumber(global.glob).value;
                }
                if(tempVal[i] == 'prok'){
                    totalPrice += RomanNumber(global.prok).value;
                }
                if(tempVal[i] == 'pish'){
                    totalPrice += RomanNumber(global.pish).value;
                }
                if(tempVal[i] == 'tegj'){
                    totalPrice += RomanNumber(global.tegj).value;
                }
            }
            global.Gold = tempPrice / totalPrice;
            // console.log('GOLD -> ', global.Gold);
        }

        if (line.search('Iron is') >= 0) {
            var tempPrice = parseInt(line.split('Iron is')[1].split('Credits')[0]);
            var tempVal   = line.split('Iron is')[0].split(' ');
            var totalPrice = 0;
            for(i=0; i<tempVal.length-1; i++){
                if(tempVal[i] == 'glob'){
                    totalPrice += RomanNumber(global.glob).value;
                }
                if(tempVal[i] == 'prok'){
                    totalPrice += RomanNumber(global.prok).value;
                }
                if(tempVal[i] == 'pish'){
                    totalPrice += RomanNumber(global.pish).value;
                }
                if(tempVal[i] == 'tegj'){
                    totalPrice += RomanNumber(global.tegj).value;
                }
            }
            global.Iron = tempPrice / totalPrice;
            // console.log('IRON -> ', global.Iron);
        }

    },

    ruleQuestion: function (line) {
        if (line.search('how much') >= 0) {
            var galacticValesArray = ["glob", "prok", "pish", "tegj"];
            var howMuchQuestion = line.split('how much')[1];
            if (searchStringInArray(howMuchQuestion, galacticValesArray) >= 0) {
                var intergalacticRomens = howMuchQuestion.split(' ?')[0].split(' is ')[1].split(' ');
                var howMuchTotalNumber = '';
                for (i = 0; i < intergalacticRomens.length; i++) {
                    if (intergalacticRomens[i] == 'glob') { howMuchTotalNumber += global.glob }
                    else if (intergalacticRomens[i] == 'prok') { howMuchTotalNumber += global.prok }
                    else if (intergalacticRomens[i] == 'pish') { howMuchTotalNumber += global.pish }
                    else if (intergalacticRomens[i] == 'tegj') { howMuchTotalNumber += global.tegj }
                    else { howMuchTotalNumber = 'I have no idea what you are talking about'; }
                }
                global.resultTxt.push(intergalacticRomens.join(' ') + ' is ' + RomanNumber(howMuchTotalNumber).value);


            } else {
                global.resultTxt.push('I have no idea what you are talking about')

            }

        }
        function searchStringInArray(str, strArray) {
            for (var j = 0; j < strArray.length; j++) {
                if (str.match(strArray[j])) return j;
            }
            return -1;
        }
        if (line.search('how many') >= 0) {
            var howManyQuestion = line.split('how many Credits is ')[1].split(' ?')[0];
            if(howManyQuestion.search('Silver') >=0){
               var tempVal = howManyQuestion.split('Silver')[0].split(' ');
               var totalPrice = 0;
               for(i=0; i<tempVal.length-1; i++){
                   if(tempVal[i] == 'glob'){
                       totalPrice += RomanNumber(global.glob).value;
                   }
                   if(tempVal[i] == 'prok'){
                       totalPrice += RomanNumber(global.prok).value;
                   }
                   if(tempVal[i] == 'pish'){
                       totalPrice += RomanNumber(global.pish).value;
                   }
                   if(tempVal[i] == 'tegj'){
                       totalPrice += RomanNumber(global.tegj).value;
                   }
               }
               global.resultTxt.push(tempVal.join(' ') + ' Silver is '+ (totalPrice * global.Silver) + ' Credits');
            }
            if(howManyQuestion.search('Iron') >=0){
                var tempVal = howManyQuestion.split('Iron')[0].split(' ');
                var totalPrice = 0;
                for(i=0; i<tempVal.length-1; i++){
                    if(tempVal[i] == 'glob'){
                        totalPrice += RomanNumber(global.glob).value;
                    }
                    if(tempVal[i] == 'prok'){
                        totalPrice += RomanNumber(global.prok).value;
                    }
                    if(tempVal[i] == 'pish'){
                        totalPrice += RomanNumber(global.pish).value;
                    }
                    if(tempVal[i] == 'tegj'){
                        totalPrice += RomanNumber(global.tegj).value;
                    }
                }
                global.resultTxt.push(tempVal.join(' ') + ' Iron is '+ (totalPrice * global.Iron) + ' Credits');
            }
            if(howManyQuestion.search('Gold') >=0){
                var tempVal = howManyQuestion.split('Gold')[0].split(' ');
                var totalPrice = 0;
                for(i=0; i<tempVal.length-1; i++){
                    if(tempVal[i] == 'glob'){
                        totalPrice += RomanNumber(global.glob).value;
                    }
                    if(tempVal[i] == 'prok'){
                        totalPrice += RomanNumber(global.prok).value;
                    }
                    if(tempVal[i] == 'pish'){
                        totalPrice += RomanNumber(global.pish).value;
                    }
                    if(tempVal[i] == 'tegj'){
                        totalPrice += RomanNumber(global.tegj).value;
                    }
                }
                global.resultTxt.push(tempVal.join(' ') + ' Gold is '+ (totalPrice * global.Gold) + ' Credits');
            }
            
        }
    }
}