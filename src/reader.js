var fs = require('fs');
var global = require('./global');



module.exports = {

    readLine: function (file_path) {
        return new Promise(
            function (resolve, reject) {
                fs.readFile(file_path, function (err, data) {
                    if (err) {
                        reject(errorcodes.PATH_ERROR)
                    } else {
                        // Split line txt file line by line and
                        // each line is assigned a global variable
                        resolve(true);
                        var array = data.toString().split("\n");
                        for (i in array) {
                            var line = array[i];
                            global.line[i] = line;
                        }
                    }
                });
            }
        )
    }



}