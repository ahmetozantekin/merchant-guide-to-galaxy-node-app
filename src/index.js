var global      = require('./global');
var reader      = require('./reader');
var writer      = require('./writer');
var linetype    = require('./linetype');
var linechecker = require('./linechecker');
var errorcodes  = require('./errorcodes');

const INPUT_PATH  = './input.txt';
const OUTPUT_PATH = './output.txt';
/*
* @MethodName  readLine  
* @Parameters  input_file_path
* @Description read the txt input file line by line
*/
reader.readLine(INPUT_PATH)
    .then(function (result) {
        for (i in global.line) {
            var line = global.line[i];
            /*
            * @MethodName  check  
            * @Parameters  line
            * @Description controller from each line
            */
            linechecker.check(line)
                .then(function(result){
                    if(result){
                        /*
                        * @MethodName  writeOutput  
                        * @Parameters  output_file_path
                        * @Description write global result in output txt file 
                        */
                        writer.writeOutput(OUTPUT_PATH)
                            .then(function(result){
                                if(result){
                                    errorcodes.SUCCES_OK;
                                } else {
                                    throw errorcodes.PATH_ERROR;     
                                }
                            })
                            .catch(function(result){
                                console.log(errorcodes.LINE_WRITE_ERROR);
                            })
                    } else {
                        console.log(errorcodes.LINE_READ_ERROR);
                    }
                })
                .catch(function(result){
                    console.log(result);     
                })
          
        }

    })
    .catch(function (result) {
        console.log(result);
    })



