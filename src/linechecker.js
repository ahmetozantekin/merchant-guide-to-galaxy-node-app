const RomanNumber = require('roman-number');
var global = require('./global');
var linetype = require('./linetype');
var errorcodes = require('./errorcodes');

module.exports = {
    check: function (line) {
        return new Promise(
            function (resolve, reject) {
                if (line.search('glob is') >= 0
                    ||
                    line.search('prok is') >= 0
                    ||
                    line.search('pish is') >= 0
                    ||
                    line.search('tegj is') >= 0) {
                    linetype.VAR(line);
                    resolve(true)
                }

                else if (line.search('Silver is') >= 0
                    ||
                    line.search('Gold is') >= 0
                    ||
                    line.search('Iron is') >= 0) {
                    linetype.METAL(line);
                    resolve(true)
                }

                else if (line.search('how much') >= 0
                    ||
                    line.search('how many') >= 0) {
                    linetype.QUESTION(line);
                    resolve(true)
                }
                else {
                    reject(errorcodes.UNDEFINED_LINE_TYPE);
                }
            }
        )

    }

}