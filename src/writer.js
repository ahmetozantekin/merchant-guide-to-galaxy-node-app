var fs = require('fs');
var global = require('./global');

module.exports = {
    writeOutput: function (file_path) {
        return new Promise(
            function (resolve, reject) {
                fs.writeFile(file_path, global.resultTxt.join('\n') , function (err) {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(true);
                    }
                });
            }
        )
    }
}