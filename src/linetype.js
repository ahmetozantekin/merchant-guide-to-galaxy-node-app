var linerules = require('./linerules');
module.exports = {

    // Global Vars,
    // glob, prok, pish, tegj
    VAR: function (e) {
        linerules.ruleVar(e);
    },

    // Metal Types,
    // Silver, Gold, Iron
    METAL: function (e) {
        linerules.ruleMetals(e);
    },


    // Question Types,
    // How many, How much
    QUESTION: function (e) {
        linerules.ruleQuestion(e);
    },


    // Error
    ERR: function (e) {
        console.log('linetype ', e)
    },
}