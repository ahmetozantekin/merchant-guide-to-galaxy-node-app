module.exports = {

    // Success
    SUCCES_OK : 'Success',

    // Wrong Path
    PATH_ERROR : 'Error - Wrong Path',

    // Read Line Error
    LINE_READ_ERROR: 'Error - Read Line',

    // Undefined Line Type
    UNDEFINED_LINE_TYPE: 'Error - Undefined Line Type',

    // Write Line Error
    LINE_WRITE_ERROR : 'Error - Write Line',
}